<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://tll.world/home.php" />
<link rel="canonical" href="https://tll.world/home.php" />
<meta property="og:title" content="Branding + Technology | THE LAUREATE LEAGUE" />
<title>Branding + Technology | THE LAUREATE LEAGUE</title>

</head>

<body class="body">
    <!--<div id='stars'></div>
    <div id='stars2'></div>
    <div id='stars3'></div>-->

<div class="background-container">
    <!--<img src="img/moon2.png" class="imgmoon">-->
    <canvas id="pixie"></canvas>
    <div class="stars2"></div>
    <!--<div class="stars"></div>
    <div class="twinkling"></div>-->
    <div class="clouds"></div>
</div>
<div class="top-index">
<div class="same-padding width100 div1 top-div text-center">
	<img src="img/logo.png" class="logo wow pulse" data-wow-iteration="infinite" data-wow-duration="16s" alt="THE LAUREATE LEAGUE" title="THE LAUREATE LEAGUE">
    <p class="top-p p-size wow fadeIn" data-wow-delay="0.6s">THE LAUREATE LEAGUE is an innovative technology company that focuses on the future of Digital Brands.
We provide the best technological solutions to enhance your business and Brand positioning.
With a strong foundation in Brands and Branding, we know what it takes to make any product stand out — and we can put this knowledge to good use for you.
</p>
</div>
<div class="clear"></div> 
    <div class="width100 same-padding overflow third-div">
    	<p class="text-center cube-p wow fadeIn" data-wow-delay="0.6s"><img src="img/icon5.png" class="cube-img wow bounce"  data-wow-iteration="infinite" data-wow-duration="16s"></p>
    	<h1 class="title2 lora title-size text-center wow fadeIn" data-wow-delay="0.9s">OUR PRODUCTS AND SERVICES</h1>
       
    </div>
    <div class="clear"></div>  
    <!--<div class="width100 same-padding overflow third-div2  third-div3">-->
<!--    <a href="https://appdev.tll.world/"  target="_blank" class="two-div-a shadow-hover blueshadow-box hover-effect">
        <div class="two-div-css wow fadeIn" data-wow-delay="1.2s">
        	<p class="icon-img-p wow fadeIn" data-wow-delay="1.4s"><img src="img/icons10.png" alt="NFT Market Place" title="NFT Market Place" class="two-icon-png  wow  pulse "  data-wow-iteration="infinite" data-wow-duration="8s"></p>
            <p class="three-div-subtitle  subtitle-size wow fadeIn" data-wow-delay="1.6s">NFT Market Place</p>
           
        </div>
    </a>
    <a  class="two-div-a blueshadow-box hover-effect shadow-hover">
        <div class="two-div-css wow fadeIn second-two-div" data-wow-delay="2.1s">
        	<p class="icon-img-p wow fadeIn" data-wow-delay="2.3s"><img src="img/icons11.png" alt="Crypto Solutions" title="Crypto Solutions" class="two-icon-png wow  pulse "  data-wow-iteration="infinite" data-wow-duration="8s"></p>
            <p class="three-div-subtitle  subtitle-size wow fadeIn" data-wow-delay="2.5s">Crypto Solutions</p>
            <p class="p-size wow fadeIn" data-wow-delay="2.7s">Coming Soon</p>
        </div>        
    </a> -->
    <div class="width100 same-padding overflow one-div-container">
    <a href="https://marketplace.tll.world/"  target="_blank" class="one-div shadow-hover blueshadow-box hover-effect">
        <div class="two-div-css wow fadeIn" data-wow-delay="1.2s">
        	<p class="icon-img-p wow fadeIn" data-wow-delay="1.4s"><img src="img/icons10.png" alt="NFT Marketplace" title="NFT Marketplace" class="one-icon-png  wow  pulse "  data-wow-iteration="infinite" data-wow-duration="8s"></p>
            <p class="three-div-subtitle  subtitle-size wow fadeIn" data-wow-delay="1.6s">NFT Marketplace</p>
           
        </div>
    </a>
    <div class="clear"></div>
    <a href="./pdf/Nidavellir_Whitepaper_v2.2_compressed.pdf" target="_blank"><div class="one-div color-button">Whitepaper</div></a>
    </div>
    <div class="clear"></div>   
<div class="same-padding width100 second-div  overflow">
    	<p class="text-center cube-p wow fadeIn" data-wow-delay="0.6s"><img src="img/icon6.png" class="cube-img wow bounce"  data-wow-iteration="infinite" data-wow-duration="16s"></p>
    	<h1 class="title2 lora title-size text-center wow fadeIn" data-wow-delay="0.5s">OUR EXPERTISE</h1>
                <p class="p-size  below-h1-p wow fadeIn margin-bottom30" data-wow-delay="0.8s">Our team will help take your company from the realm of mediocrity to an interactive path to success. By fusing technology and Branding, we will help you create a dynamic presence in today's digital marketplace.</p>
   		<div class="five-div-css first-five-div">
        	<img src="img/job1.png" class="five-img wow fadeIn" data-wow-delay="1.1s" alt="Strategists" title="Strategists">
            <p class="p-size three-div-p wow fadeIn" data-wow-delay="1.3s">Strategists</p>
        </div>
   		<div class="five-div-css second-five-div">
        	<img src="img/job2.png" class="five-img wow fadeIn" data-wow-delay="1.5s" alt="Designers" title="Designers">
            <p class="p-size three-div-p wow fadeIn" data-wow-delay="1.7s">Designers</p>
        </div>        
   		<div class="five-div-css third-five-div">
        	<img src="img/job3.png" class="five-img wow fadeIn" data-wow-delay="1.9s" alt="Writers" title="Writers">
            <p class="p-size three-div-p wow fadeIn" data-wow-delay="2.1s">Writers</p>
        </div>     
   		<div class="five-div-css four-five-div">
        	<img src="img/job4.png" class="five-img wow fadeIn" data-wow-delay="2.3s" alt="Developers" title="Developers">
            <p class="p-size three-div-p wow fadeIn" data-wow-delay="2.5s">Developers</p>
        </div>         
   		<div class="five-div-css last-five-div">
        	<img src="img/job5.png" class="five-img wow fadeIn" data-wow-delay="2.7s" alt="Marketing Experts" title="Marketing Experts">
            <p class="p-size three-div-p wow fadeIn" data-wow-delay="2.9s">Marketing Experts</p>
        </div>        
                


        
    </div>

</div>
    <div class="clear"></div>

    <div class="width100 same-padding overflow third-div">
    	<p class="text-center cube-p wow fadeIn" data-wow-delay="0.6s"><img src="img/icon1.png" class="cube-img wow bounce"  data-wow-iteration="infinite" data-wow-duration="16s"></p>
    	<h1 class="title2 lora title-size text-center wow fadeIn" data-wow-delay="0.9s">POWER BRANDS</h1>
    </div>
    <div class="clear"></div>
    <div class="width100 same-padding overflow third-div2">
        <div class="three-div-css blueshadow-box wow fadeIn" data-wow-delay="1.2s">
        	<p class="icon-img-p wow fadeIn" data-wow-delay="1.4s"><img src="img/icons1.png" alt="Technology" title="Technology" class="icon-png  wow  pulse "  data-wow-iteration="infinite" data-wow-duration="8s"></p>
            <p class="three-div-subtitle  subtitle-size wow fadeIn" data-wow-delay="1.6s">Technology</p>
            <p class="p-size three-div-p wow fadeIn" data-wow-delay="1.8s">The Laureate League’s combination of technology and Branding will help Brands become Power Brands in their own metaverses.</p>
        </div>
        <div class="three-div-css blueshadow-box wow fadeIn center-three-div" data-wow-delay="2.1s">
        	<p class="icon-img-p wow fadeIn" data-wow-delay="2.3s"><img src="img/icons2.png" alt="Branding" title="Branding" class="icon-png wow  pulse "  data-wow-iteration="infinite" data-wow-duration="8s"></p>
            <p class="three-div-subtitle  subtitle-size wow fadeIn" data-wow-delay="2.5s">Branding</p>
            <p class="p-size three-div-p wow fadeIn" data-wow-delay="2.7s">We build powerful Brands that transcend the digital space, and help businesses become Power Brands</p>
        </div>        
        <div class="three-div-css blueshadow-box wow fadeIn" data-wow-delay="3s">
        	<p class="icon-img-p wow fadeIn" data-wow-delay="3.2s"><img src="img/icons3.png" alt="Audiences" title="Audiences" class="icon-png wow  pulse "  data-wow-iteration="infinite" data-wow-duration="8s"></p>
            <p class="three-div-subtitle  subtitle-size wow fadeIn" data-wow-delay="3.4s">Audiences</p>
            <p class="p-size three-div-p wow fadeIn" data-wow-delay="3.6s">So that they too will be able to harness the power of humanity to grow, evolve, and win over their audiences.</p>
        </div>             
    </div>

    <div class="clear"></div>    
    <div class="width100 same-padding tech-div">
    	<div class="two2-div left-div">
        	<div class="left-cube-div">
            	<p class="text-center cube-p wow fadeIn" data-wow-delay="0.6s"><img src="img/icon2.png" class="cube-img wow bounce"  data-wow-iteration="infinite" data-wow-duration="16s"></p>
            </div>
            <div class="right-text-div1">
            	<h1 class="title3 lora title-size text-center wow fadeIn" data-wow-delay="0.9s">TECHNOLOGY AND
BRANDS</h1>
            </div>
        </div>
        <div class="two2-div right-div">
        	<p class="p-size wow fadeIn right-p2" data-wow-delay="1.2s">We have heard these buzzwords bandied about by hundreds of marketeers, but few actually understand their interrelationship.<br><br>

The Internet brings to us to the realms of technology wonders. Metaverses, NFTS and Cryptos are no longer stuff of science fiction, but part of the new digital economy.<br><br> 

As a technology Brand, Laureate League will be embarking on new digital spaces that will provide you the best of technological solutions to enhance your business and Brand positioning. </p>
        </div>
    
    
    
    </div>
    <div class="clear"></div>
  
    <div class="width100 same-padding table-div">
    	<div class="two2-div left-div wow bounceInLeft" data-wow-delay="0.9s" data-wow-duration="3s">
        	<p class="table-subtitle  subtitle-size">OUR PHILOSOPHY</p>
        	<table class="table2">
            	<tr>
                	<td class="number-td">1</td>
                    <td class="p-size next-to-number-td">The Laureate League's framework for digital transformation aims to elevates Brands to the next level.</td>
                </tr>
            	<tr>
                	<td class="number-td">2</td>
                    <td class="p-size next-to-number-td">We set out to uncover the answers behind technology and Branding, with new innovations that will benefit human kind and help to build a better inclusive world.</td>
                </tr>                
            	<tr>
                	<td class="number-td">3</td>
                    <td class="p-size next-to-number-td">Our insights into technology and its relationship with Branding will reshape the way Brands leverage technology in their Branding strategies.</td>
                </tr>                  
            </table>
        </div>
        <div class="two2-div right-div wow bounceInRight" data-wow-delay="0.9s" data-wow-duration="3s">
        	<p class="table-subtitle  subtitle-size">OUR VISION</p>
        	<table class="table2">
            	<tr>
                	<td class="number-td">1</td>
                    <td class="p-size next-to-number-td">To build an eco-system of digital solutions that enable Brands to seek new grounds of growth and stay relevant to the needs of the community.</td>
                </tr>
            	<tr>
                	<td class="number-td">2</td>
                    <td class="p-size next-to-number-td">Technology is the best tool Brands can use to enhance their digital presence and build their Brand's presence online.</td>
                </tr>                
            	<tr>
                	<td class="number-td">3</td>
                    <td class="p-size next-to-number-td">We combine the best of Branding, and technological solutions to make your business stand out, to help your Brand achieve the following results:
                    <ul class="table-td-ul">
                    	<li>Position your Brand in a better place in the minds of consumers</li>
                        <li>Stand out from competition</li>
                        <li>Provide consumers with an experience to remember</li></ul></td>
                </tr>                  
            </table>        	
        </div>
    
    
    
    </div>    
     <div class="clear"></div>

    <div class="width100 same-padding overflow third-div">
    	<p class="text-center cube-p wow fadeIn" data-wow-delay="0.6s"><img src="img/icon3.png" class="cube-img wow bounce"  data-wow-iteration="infinite" data-wow-duration="16s"></p>
    	<h1 class="title2 lora title-size text-center wow fadeIn" data-wow-delay="0.9s">OUR MISSION</h1>
    </div>
    <div class="clear"></div>
    <div class="width100 same-padding overflow third-div2">
        <div class="three-div-css blueshadow-box wow fadeIn" data-wow-delay="1.2s">
        	<p class="icon-img-p wow fadeIn" data-wow-delay="1.4s"><img src="img/icons4.png" alt="Solution" title="Solution" class="icon-png  wow  pulse "  data-wow-iteration="infinite" data-wow-duration="8s"></p>
            <p class="three-div-subtitle2  subtitle-size wow fadeIn" data-wow-delay="1.6s">Solution</p>
            <p class="p-size three-div-p wow fadeIn" data-wow-delay="1.8s">As both an agency and developer, The Laureate League’s mission is building solutions that perfectly match your Brand's needs and goals to achieve maximum return on your IT investment.</p>
        </div>
        <div class="three-div-css blueshadow-box wow fadeIn center-three-div" data-wow-delay="2.1s">
        	<p class="icon-img-p wow fadeIn" data-wow-delay="2.3s"><img src="img/icons5.png" alt="E-Commerce" title="E-Commerce" class="icon-png wow  pulse "  data-wow-iteration="infinite" data-wow-duration="8s"></p>
            <p class="three-div-subtitle2  subtitle-size wow fadeIn" data-wow-delay="2.5s">E-Commerce</p>
            <p class="p-size three-div-p wow fadeIn" data-wow-delay="2.7s">We also work with leading third-party e-commerce platforms to create seamless customer experiences.</p>
        </div>        
        <div class="three-div-css blueshadow-box wow fadeIn" data-wow-delay="3s">
        	<p class="icon-img-p wow fadeIn" data-wow-delay="3.2s"><img src="img/icons6.png" alt="Business Development" title="Business Development" class="icon-png wow  pulse "  data-wow-iteration="infinite" data-wow-duration="8s"></p>
            <p class="three-div-subtitle2  subtitle-size wow fadeIn" data-wow-delay="3.4s">Business Development</p>
            <p class="p-size three-div-p wow fadeIn" data-wow-delay="3.6s">In short, we will handle all of the hard stuff, leaving you to focus on building and developing your business and Brand positioning.</p>
        </div>             
    </div>  
    <div class="clear"></div>


    <div class="width100 same-padding overflow third-div">
    	<p class="text-center cube-p wow fadeIn" data-wow-delay="0.6s"><img src="img/icon4.png" class="cube-img wow bounce"  data-wow-iteration="infinite" data-wow-duration="16s"></p>
    	<h1 class="title2 lora title-size text-center wow fadeIn" data-wow-delay="0.9s">CONSULT US NOW</h1>
        <p class="p-size below-h1-p wow fadeIn" data-wow-delay="1.1s">Laureate League is the go-to company that will change the way you perceive yourself, your business, your sales, and your bottom line.
If you want your Brand to be one of the best, check us out right now. Another forward moving initiative of <a href="https://www.thebrandlaureate.com/" target="_blank" class="light-blue-link hover-effect">The BrandLaureate</a>.</p>
    </div>
    <div class="clear"></div>
    <div class="width100 same-padding overflow third-div2" style="margin-bottom:50px !important;">
        <div class="three-div-css blueshadow-box wow fadeIn" data-wow-delay="1.2s">
        	<p class="icon-img-p wow fadeIn" data-wow-delay="1.4s"><img src="img/icons7.png" alt="Company" title="Company" class="icon-png  wow  pulse "  data-wow-iteration="infinite" data-wow-duration="8s"></p>
            <p class="three-div-subtitle  subtitle-size wow fadeIn" data-wow-delay="1.6s">Company</p>
            <p class="p-size three-div-p wow fadeIn" data-wow-delay="1.8s" style="text-align:center !important;">THE LAUREATE LEAGUE SDN. BHD.<br>202101019357 (1419657-X)</p>
        </div>
        <div class="three-div-css blueshadow-box wow fadeIn center-three-div" data-wow-delay="2.1s">
        	<p class="icon-img-p wow fadeIn" data-wow-delay="2.3s"><img src="img/icons8.png" alt="Email" title="Email" class="icon-png wow  pulse "  data-wow-iteration="infinite" data-wow-duration="8s"></p>
            <p class="three-div-subtitle  subtitle-size wow fadeIn" data-wow-delay="2.5s">Contact & Email</p>
            <p class="p-size three-div-p wow fadeIn" data-wow-delay="2.7s" style="text-align:center !important;">03-7710 0348<br>info@tll.world</p>
            <p class="icon-p"><a href="https://discord.gg/9kYuvqfe" target="_blank" class="social-icon wow fadeIn" data-wow-delay="2.9s"><img src="img/discord.png" ></a><a href="https://www.youtube.com/channel/UCwsjBxfyjbUuzlSQKnGnjdA" target="_blank" class="social-icon wow fadeIn" data-wow-delay="3.1s"><img src="img/youtube.png" ></a><a href="https://www.facebook.com/TheLaureateLeague" target="_blank" class="social-icon wow fadeIn last-social-icon" data-wow-delay="3.3s"><img src="img/fb.png" ></a><br><br><a href="https://t.me/tllworld" target="_blank" class="social-icon wow fadeIn" data-wow-delay="3.5s"><img src="img/telegram.png"></a><a href="https://www.instagram.com/thelaureateleague/" target="_blank" class="social-icon wow fadeIn" data-wow-delay="3.7s"><img src="img/insta.png"></a><a href="https://twitter.com/LeagueLaureate?t=MT3A8uJxlvkGxdTd4-Hkew&s=08" target="_blank" class="social-icon last-social-icon wow fadeIn" data-wow-delay="3.9s"><img src="img/twitter.png"></a></p>
        </div>        
        <div class="three-div-css blueshadow-box wow fadeIn" data-wow-delay="4.1s">
        	<p class="icon-img-p wow fadeIn" data-wow-delay="4.3s"><img src="img/icons9.png" alt="Address" title="Address" class="icon-png wow  pulse "  data-wow-iteration="infinite" data-wow-duration="8s"></p>
            <p class="three-div-subtitle  subtitle-size wow fadeIn" data-wow-delay="4.5s">Address</p>
            <p class="p-size three-div-p wow fadeIn" data-wow-delay="4.7s" style="text-align:center !important;">39B and 41B, Jalan SS 21/60, Damansara Utama, 47400 Petaling Jaya, Selangor</p>
        </div>             
    </div>
    <div class="clear"></div>      


    
   
          
<div class="footer-div footer-bg text-center">
	<p class="white-text footer-p">Copyright © <?php echo $time ;?> THE LAUREATE LEAGUE, All Rights Reserved.</p>
</div>
</div>
<?php include 'js.php'; ?>
</body>
</html>