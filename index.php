<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<meta property="og:url" content="https://tll.world/" />
<link rel="canonical" href="https://tll.world/" />
<meta http-equiv = "refresh" content = "10; url = ./home.php" />
<meta property="og:title" content="Branding + Technology | THE LAUREATE LEAGUE" />
<title>Branding + Technology | THE LAUREATE LEAGUE</title>

</head>

<body class="body" onclick="location.href='./home.php'" style="cursor:pointer;">
    <!--<div id='stars'></div>
    <div id='stars2'></div>
    <div id='stars3'></div>-->

<div class="background-container" id="containerx">
    <!--<img src="img/moon2.png" class="imgmoon">-->
    <canvas id="pixie"></canvas>
    <div class="stars2 ow-stars2"></div>
    <!--<div class="stars"></div>
    <div class="twinkling"></div>-->
    <div class="clouds"></div>
</div>
<div class="top-index text-center">

		<img src="img/logo-animation.gif" class="logo-ani" alt="THE LAUREATE LEAGUE" title="THE LAUREATE LEAGUE">


</div>
<?php include 'js.php'; ?>

</body>
</html>