<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta property="og:type" content="website" />
<meta property="og:image" content="https://vidatechft.com/hosting-picture/tll-fb-meta.jpg" />
<meta name="author" content="THE LAUREATE LEAGUE">
<meta property="og:description" content="THE LAUREATE LEAGUE is an innovative technology company that focuses on the future of Digital Brands." />
<meta name="description" content="THE LAUREATE LEAGUE is an innovative technology company that focuses on the future of Digital Brands." />
<meta name="keywords" content="THE LAUREATE LEAGUE, tll, technology, digital, branding, corperate branding, Brands, The Brand Laureate, marketing, etc">
<?php
  $tz = 'Asia/Kuala_Lumpur';
  $timestamp = time();
  $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
  $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
  $time = $dt->format('Y');
?>
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
<link rel="icon" href="img/favicon.ico" type="image/x-icon">
<link href="https://fonts.googleapis.com/css2?family=VT323&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/component.css">
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="css/animate.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/style.css?version=0.1.4">